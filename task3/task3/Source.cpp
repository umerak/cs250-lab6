
#include <iostream>
#include <random>
#include <stdio.h>
#include <time.h>
#include <vector>
#include <assert.h>
#include <algorithm>

using namespace std;
void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}
void moveMin(vector<int> &in, vector<int> &out)
{
	
	out = in;
	bool flag = false;
	int j = out.size() - 1;
	for (int i = 0; i <= j; ++i)
	{
		if (i == j)
		{
			if (!flag)
				break;
			else
			{
				flag = false;
				i = 0;
				--j;
			}

		}
		if (out[i] > out[i + 1])
		{
			swap(out[i], out[i + 1]);
			flag = true;
		}


	}
	
}
bool testmovMin(vector<int> &in, vector<int> &out) {
	bool issorted = false;
	for (int i = 0; i < in.size(); ++i) {
		cout << in[i] <<"   "<< out[i] << endl;
		
		issorted = true;

	}
	return issorted;
}


int main() {
	clock_t tStart = clock();
	srand(time(NULL));
	vector < int> in;
	vector <int> out;

	for (int i = 0; i < 1000; i++) {
		int out = (rand() % 100) + 1;;
		in.push_back(out);
	}
	moveMin(in, out);

	sort(in.begin(), in.end());
	for (int i = 0; i < 1000; i++) {
		cout << in[i] << "  " << out[i] << endl;
	}
	

	cout << "test passed and output is" << testmovMin(in, out) << " ";
	printf("Time taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
	system("pause");
}
