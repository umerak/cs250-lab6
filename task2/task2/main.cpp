#include <iostream>
#include <random>
#include <time.h>
#include <vector>
#include <assert.h>
#include <algorithm>

using namespace std;
void moveMin(vector<int> &in, vector<int> &out)
{
	out = in;
	int temp;
	for (int i = 0; i < out.size(); ++i) {
		for (int j = i + 1; j < out.size() - 1; ++j)
		{
			if (out[i] > out[j])
			{
				temp = out[i];
				out[i] = out[j];
				out[j] = temp;
			}

		}


	}
}
bool testmovMin(vector<int> &in, vector<int> &out) {
	bool issorted = false;
	for (int i = 0; i < in.size(); ++i) {
		cout << in[i] << out[i] << endl;
		assert(in[i] == out[i]);
		issorted = true;

	}
	return issorted;
}


int main() {
	clock_t tStart = clock();
	srand(time(NULL));
	vector < int> in;
	vector <int> out;

	for (int i = 0; i < 1000; i++) {
		int out = (rand() % 100) + 1;
		in.push_back(out);
	}
	moveMin(in, out);

	sort(in.begin(), in.end());

	in.push_back((rand() % 100) + 1);
	out.push_back((rand() % 100) + 1);
	moveMin(in, out);

	cout << "test passed and output is" << testmovMin(in, out) << " ";
	printf("Time taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
	system("pause");
}
